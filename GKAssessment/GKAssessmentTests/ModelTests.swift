import Foundation
import XCTest
@testable import GKAssessment

class ModelTests: XCTestCase
{
    var jsonQuestionDict : [String: Any]?
    
    override func setUp()
    {
        super.setUp()
        jsonQuestionDict = ["tags":["json","swift","parsing","swift3","xcode8"],
                                   "owner":["reputation":638,"user_id":2563039,"user_type":"registered","accept_rate":85,"profile_image":"https://www.gravatar.com/avatar/988315a438462497ad4755326a275786?s=128&d=identicon&r=PG","display_name":"user2563039","link":"https://stackoverflow.com/users/2563039/user2563039"],"is_answered":true,"view_count":112559,"protected_date":1533654912,"accepted_answer_id":39423764,"answer_count":6,"score":99,"last_activity_date":1539689597,"creation_date":1473489795,"last_edit_date":1495540499,"question_id":39423367,"link":"https://stackoverflow.com/questions/39423367/correctly-parsing-json-in-swift-3","title":"Correctly Parsing JSON in Swift 3","body":"<p>I'm trying to fetch a JSON response and store the results in a variable. I've had versions of this code work in previous releases of Swift, until the GM version of Xcode 8 was released. I had a look at a few similar posts on StackOverflow: <a href=\"https://stackoverflow.com/questions/33324766/swift-2-parsing-json-cannot-subscript-a-value-of-type-anyobject?noredirect=1&amp;lq=1\">Swift 2 Parsing JSON - Cannot subscript a value of type 'AnyObject'</a> and <a href=\"https://stackoverflow.com/questions/38155436/json-parsing-in-swift-3\">JSON Parsing in Swift 3</a>. </p>\n\n<p>However, it seems the ideas conveyed there do not apply in this scenario.</p>\n\n<p>How do I correctly parse the JSON reponse in Swift 3? \nHas something changed in the way JSON is read in Swift 3?</p>\n\n<p>Below is the code in question (it can be run in a playground):</p>\n\n<pre><code>import Cocoa\n\nlet url = \"https://api.forecast.io/forecast/apiKey/37.5673776,122.048951\"\n\nif let url = NSURL(string: url) {\n    if let data = try? Data(contentsOf: url as URL) {\n        do {\n            let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)\n\n        //Store response in NSDictionary for easy access\n        let dict = parsedData as? NSDictionary\n\n        let currentConditions = \"\\(dict![\"currently\"]!)\"\n\n        //This produces an error, Type 'Any' has no subscript members\n        let currentTemperatureF = (\"\\(dict![\"currently\"]![\"temperature\"]!!)\" as NSString).doubleValue\n\n            //Display all current conditions from API\n            print(currentConditions)\n\n            //Output the current temperature in Fahrenheit\n            print(currentTemperatureF)\n\n        }\n        //else throw an error detailing what went wrong\n        catch let error as NSError {\n            print(\"Details of JSON parsing error:\\n \\(error)\")\n        }\n    }\n}\n</code></pre>\n\n<p><strong>Edit:</strong> Here is a sample of the results from the API call after <code>print(currentConditions)</code></p>\n\n<pre><code>[\"icon\": partly-cloudy-night, \"precipProbability\": 0, \"pressure\": 1015.39, \"humidity\": 0.75, \"precipIntensity\": 0, \"windSpeed\": 6.04, \"summary\": Partly Cloudy, \"ozone\": 321.13, \"temperature\": 49.45, \"dewPoint\": 41.75, \"apparentTemperature\": 47, \"windBearing\": 332, \"cloudCover\": 0.28, \"time\": 1480846460]\n</code></pre>\n"]
       
    }
    
    func testQuestionCreatebody()
    {
        if let jsonQuestionDict  = jsonQuestionDict
        {
            let question = Question(jsonDictionary: jsonQuestionDict)
            XCTAssertTrue(question.body != nil)
        }
    }
    
    
    
    func testQuestionerCreate()
    {
        if let jsonQuestionDict  = jsonQuestionDict
        {
            let question = Question(jsonDictionary: jsonQuestionDict)
            XCTAssertTrue(question.owner.profileImage != nil)
        }
    }
}

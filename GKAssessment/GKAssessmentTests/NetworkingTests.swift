//
//  NetworkingTests.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import Foundation
import XCTest
@testable import GKAssessment

class NetworkingTests: XCTestCase {
    
    
    func testBuildingURLParameters()
    {
       let query = "ios"
        let url = FetchQuestions().buildURLWithComponents(query: query)
        XCTAssertTrue(url?.query?.contains(query) ?? false)
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSearchingForQuestion()
    {
        let searchQuery = "IOS"
        FetchQuestions().search(query: searchQuery, completionHandler: { (result, error) in
                XCTAssertTrue(result.count > 0)
            })
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}


import Foundation
import UIKit
extension UIColor
{
    convenience init(hexString: String)
    {
        var incomingHex: String!
        incomingHex = hexString
        
        let hex = incomingHex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count
        {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension String
{
    var convertHtmlToAttributedString: String?
    {
        do {
            if let data = data(using: String.Encoding.utf8, allowLossyConversion: true)
            {
                let attributedString = try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                return attributedString.string
            }
        }
        catch
        {
            print("An Error occurred :\(error)")
        }
        return nil
    }
}

extension UIImage
{
    func resizeImage(size: CGSize)-> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: size.width, height: size.height), false, 0)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
}

extension UIViewController
{
    class func displayActivityIndicator(inView : UIView) -> UIView
    {
        let spinner = UIView.init(frame: inView.bounds)
        spinner.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator.startAnimating()
        activityIndicator.center = spinner.center
        
        DispatchQueue.main.async
        {
            spinner.addSubview(activityIndicator)
            inView.addSubview(spinner)
        }
        
        return spinner
    }
    
    class func removeActivityIndicator(indicator :UIView)
    {
        DispatchQueue.main.async
        {
            indicator.removeFromSuperview()
        }
    }
}

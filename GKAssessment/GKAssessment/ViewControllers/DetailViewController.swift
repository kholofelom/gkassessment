//
//  DetailViewController.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var ownerDetailsStackView: UIStackView!
    @IBOutlet weak var ownerImageView: UIImageView!
    @IBOutlet weak var ownerStackView: UIStackView!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var questionTags: UITextView!
    
    @IBOutlet weak var questionDetail: UITextView!
    
    var reputation  : UILabel =
    {
        let reputation = UILabel()
        reputation.textColor = UIColor(hexString: "#8F8E94")
        reputation.font = reputation.font.withSize(10)
        return reputation
    }()
    
    var creationDate  : UILabel =
    {
        let creationDate = UILabel()
        creationDate.textColor = UIColor(hexString: "#8F8E94")
        creationDate.font = creationDate.font.withSize(10)
        return creationDate
    }()
    
    var owner  : UILabel =
    {
        let owner = UILabel()
        owner.textColor = UIColor(hexString: "#8F8E94")
        owner.font = owner.font.withSize(10)
        return owner
    }()
    
    func configureView()
    {
        if let detailItem = detailItem, let _ = self.view
        {
            headerLabel.textColor = UIColor(hexString: "#727272")
            headerLabel.attributedText = NSMutableAttributedString(string:String(detailItem.title),attributes: [.font: UIFont.boldSystemFont(ofSize: 17)])
            let tagsString = (detailItem.tags.map{String($0)}).joined(separator: ",")
            
            questionTags.attributedText = NSMutableAttributedString(string: tagsString, attributes: [.font: UIFont.boldSystemFont(ofSize: 10)])
            questionTags.textColor = UIColor(hexString: "#8F8E94")
            
            if let body = detailItem.body
            {
              questionDetail.text = body
            }
            
            owner.text = detailItem.owner.displayName
            ownerDetailsStackView.addArrangedSubview(owner)
            reputation.attributedText = NSMutableAttributedString(string:String(detailItem.owner.reputation),attributes: [.font: UIFont.boldSystemFont(ofSize: 10)])
            ownerDetailsStackView.addArrangedSubview(reputation)
            creationDate.text = convertDateToString(date: detailItem.creationDate)
            ownerDetailsStackView.addArrangedSubview(creationDate)
            
            if let imageURL = detailItem.owner.profileImage
            {
                FetchQuestions().fetchData(url: imageURL, completionHandler: { (data, error) in
                    if let data = data
                    {
                        DispatchQueue.main.async
                        {
                            self.ownerImageView.image = UIImage(data:data)
                        }
                    }
                })
            }
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        configureView()
    }
    
    var detailItem: Question? {
        didSet {
            // Update the view.
            configureView()
        }
    }
}

extension DetailViewController
{
    func convertDateToString(date: Date) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy HH:mm"
        
        let stringDate: String = dateFormatter.string(from: date)
        return stringDate
    }
    
}


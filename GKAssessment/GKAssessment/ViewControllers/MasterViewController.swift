//
//  MasterViewController.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    var searchBar = UISearchBar()
    var detailViewController: DetailViewController? = nil
    var questions = [Question]()
    var searchTextOffset = UIOffset()
    var splitViewCollapsed = false
    
    var introLabel : UILabel =
    {
        let introLabel = UILabel()
        introLabel.textColor = UIColor(hexString: "#727272")
        introLabel.attributedText = NSMutableAttributedString(string: "Stack Overflow Search", attributes: [.font: UIFont.boldSystemFont(ofSize: 15)])
        return introLabel
    }()
    
    
    lazy var questionsTableView : UITableView =
        {
            let questionsTableView = UITableView()
            questionsTableView.delegate = self
            questionsTableView.dataSource = self
            questionsTableView.register(QuestionCellView.self, forCellReuseIdentifier: "questionCell")
            questionsTableView.rowHeight = 80
            questionsTableView.separatorStyle = .none
            return questionsTableView
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addTableView()
        addIntroLabel()
        setupSearchBar()
        if let split = splitViewController
        {
            split.delegate = self
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        searchBar.isHidden = false
    }
    
    func addTableView()
    {
        self.view.addSubview(questionsTableView)
        questionsTableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraint(NSLayoutConstraint(item: questionsTableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: questionsTableView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: questionsTableView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: questionsTableView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0))
    }
    
    func addIntroLabel()
    {
        questionsTableView.addSubview(introLabel)
        
        introLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraint(NSLayoutConstraint(item: introLabel, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant:0))
        self.view.addConstraint(NSLayoutConstraint(item: introLabel, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: 0))
    }
    
    func setupSearchBar()
    {
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.barTintColor = UIColor(hexString: "#4078C4")
        
        if let navigatioNar = self.navigationController?.navigationBar
        {
            navigatioNar.addSubview(searchBar)
            searchBar.translatesAutoresizingMaskIntoConstraints = false
            navigatioNar.addConstraint(NSLayoutConstraint(item: searchBar, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: navigatioNar, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0))
            navigatioNar.addConstraint(NSLayoutConstraint(item: searchBar, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: navigatioNar, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 8))
            navigatioNar.addConstraint(NSLayoutConstraint(item: searchBar, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: navigatioNar, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: -8))
            navigatioNar.addConstraint(NSLayoutConstraint(item: searchBar, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: navigatioNar, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: -4))
        }
    }
    
    //
    //    // MARK: - Segues
    //
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showDetail"
        {
            if let indexPath = questionsTableView.indexPathForSelectedRow
            {
                let object = questions[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                if (splitViewCollapsed)
                {
                    searchBar.isHidden = true
                }
            }
        }
    }
    //
    //    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath) as? QuestionCellView
        
        if let dequeuedCell = cell
        {
            dequeuedCell.populateViews(question: questions[indexPath.row])
            return dequeuedCell
        }
        
        return UITableViewCell()
    }
}

extension MasterViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if(searchText.count > 0)
        {
            searchBar.showsCancelButton = true
            searchBar.searchTextPositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
        }
        else
        {
            searchBar.showsCancelButton = false
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text = ""
        searchBar.showsCancelButton = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        let activityIndicator = UIViewController.displayActivityIndicator(inView: self.view)
        if let searchQuery = searchBar.text
        {
            FetchQuestions().search(query: searchQuery, completionHandler: { (result, error) in
                UIViewController.removeActivityIndicator(indicator: activityIndicator)
                if let _ = error
                {
                    return
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        self.questions = result
                        self.questionsTableView.isHidden = false
                        self.questionsTableView.separatorStyle = .singleLine
                        self.introLabel.isHidden = true
                        self.detailViewController?.detailItem = self.questions[0]
                        self.questionsTableView.reloadData()
                    }
                }
            })
        }
        self.view.endEditing(true)
    }
}

extension MasterViewController : UISplitViewControllerDelegate
{
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool
    {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController
            else
        { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController
            else { return false }
        if topAsDetailController.detailItem == nil
        {
            splitViewCollapsed = true
            return true
        }
        return false
    }
}




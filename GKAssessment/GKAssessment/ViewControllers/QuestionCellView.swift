
import UIKit
import Foundation

class QuestionCellView : UITableViewCell
{
    var cellStackView: UIStackView =
    {
        let stackView = UIStackView()
        stackView.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        stackView.axis = .horizontal
        stackView.alignment = .leading
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()
    
    var detailStackView: UIStackView =
    {
        let detailStackView = UIStackView()
        detailStackView.axis = .vertical
        detailStackView.alignment = .leading
        detailStackView.distribution = .equalSpacing
        detailStackView.spacing = 5
        return detailStackView
    }()
    
    var title : UILabel =
    {
        let title = UILabel()
            title.textColor = UIColor(hexString: "#4078C4")
            title.font = title.font.withSize(14)
            title.numberOfLines = 3
            return title
    }()
    
    var votes : UILabel =
    {
        let votes = UILabel()
        votes.textColor = UIColor(hexString: "#8F8E94")
        votes.font = votes.font.withSize(10)
        return votes
    }()
    var answers  : UILabel =
    {
        let answers = UILabel()
        answers.textColor = UIColor(hexString: "#8F8E94")
        answers.font = answers.font.withSize(10)
        return answers
    }()
    
    var views  : UILabel =
    {
        let views = UILabel()
        views.textColor = UIColor(hexString: "#8F8E94")
        views.font = views.font.withSize(10)
        return views
    }()
    
    var owner  : UILabel =
    {
        let owner = UILabel()
        owner.textColor = UIColor(hexString: "#8F8E94")
        owner.font = owner.font.withSize(10)
        return owner
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .default
        self.accessoryType = .disclosureIndicator
        self.imageView?.image = UIImage()
        self.contentView.addSubview(cellStackView)
        self.contentView.addSubview(owner)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.cellStackView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addConstraint(NSLayoutConstraint(item: cellStackView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.contentView, attribute: NSLayoutAttribute.top, multiplier: 1, constant:8))
        
            self.contentView.addConstraint(NSLayoutConstraint(item: cellStackView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.imageView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 8))

        self.contentView.addConstraint(NSLayoutConstraint(item: cellStackView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.contentView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 8))
        
        //Owner Label Constraints
        self.owner.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addConstraint(NSLayoutConstraint(item: cellStackView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.owner, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: owner, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.imageView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: owner, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.contentView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 8))
        self.contentView.addConstraint(NSLayoutConstraint(item: owner, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.contentView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0))
        
    }
    
    func populateViews(question: Question)
    {
        if question.answerCount > 0
        {
            self.imageView?.image = UIImage(named: "check.png")?.resizeImage(size: CGSize(width: 15, height: 18))
        }
        answers.text = "\(String(question.answerCount)) answers"
        
        title.text = question.title
        views.text = "\(String(question.viewCount)) views "
        votes.text = "\(String(question.score)) Votes"
        owner.text = "asked by \(question.owner.displayName ?? "" )"
        
        detailStackView.addArrangedSubview(votes)
        detailStackView.addArrangedSubview(answers)
        detailStackView.addArrangedSubview(views)
        
        cellStackView.addArrangedSubview(title)
        cellStackView.addArrangedSubview(detailStackView)
    }
    
}

//
//  Question.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import Foundation

class Question: NSObject
{
    var tags: [String]
    var isAnswered = false
    var viewCount = 0
    var answerCount = 0
    var score = 0
    var creationDate : Date
    var link : URL?
    var title : String
    var owner: Questioner
    var body : String?
    
    
    init(jsonDictionary: Dictionary<String, Any>)
    {
        self.tags = jsonDictionary["tags"] as? [String] ?? [String]()
        self.isAnswered = jsonDictionary["is_answered"] as? Bool ?? false
        self.viewCount = jsonDictionary["view_count"] as? Int ?? 0
        self.answerCount = jsonDictionary["answer_count"] as? Int ?? 0
        self.score = jsonDictionary["score"] as? Int ?? 0
        self.creationDate = jsonDictionary["creation_date"] as? Date ?? Date()
        self.title = (jsonDictionary["title"] as? String)?.convertHtmlToAttributedString ?? ""
        self.body = (jsonDictionary["body"] as? String)?.convertHtmlToAttributedString
        self.owner = Questioner(jsonDictionary: jsonDictionary["owner"] as? Dictionary<String,AnyObject> ?? [String:AnyObject]())
    }
}


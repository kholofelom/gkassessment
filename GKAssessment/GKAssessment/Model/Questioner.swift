//
//  Questionaer.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import Foundation
class Questioner: NSObject
{
    var reputation = 0
    var userId: Double = 0
    var userType: String?
    var profileImage : URL?
    var displayName : String?
    var link : URL?
    
    init(jsonDictionary: Dictionary<String, Any>)
    {
        self.reputation = jsonDictionary["reputation"] as? Int ?? 0
        self.userId = jsonDictionary["user_id"] as? Double ?? 0
        self.displayName = jsonDictionary["display_name"] as? String ?? ""
        self.profileImage = URL(string:jsonDictionary["profile_image"] as? String ?? "")
        self.userType = jsonDictionary["user_type"] as? String
        self.link = URL(string:jsonDictionary["link"] as? String ?? "")
    }
}

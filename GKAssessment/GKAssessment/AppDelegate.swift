//
//  AppDelegate.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let splitViewController = window!.rootViewController as! UISplitViewController
        splitViewController.preferredDisplayMode = .allVisible
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        let barAppearance = UINavigationBar.appearance()
            barAppearance.titleTextAttributes = textAttributes
            barAppearance.tintColor = .white
        barAppearance.barTintColor = UIColor(hexString: "#4078C4")
        return true
    }

}


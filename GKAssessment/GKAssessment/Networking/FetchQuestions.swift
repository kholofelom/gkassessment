//
//  networking.swift
//  GKAssessment
//
//  Created by Kholofelo on 2018/10/13.
//  Copyright © 2018 FNB. All rights reserved.
//

import Foundation

class FetchQuestions :  NSObject, URLSessionTaskDelegate, URLSessionDataDelegate
{
    
    func search(query: String, completionHandler: @escaping ([Question], Error?) -> Void)
    {
        let url = buildURLWithComponents(query: query)
        
        var receivedError: Error?
        var questionResult = [Question]()
        
        if let url = url
        {
            fetchData(url: url, completionHandler: { (data, error) in
                if let data = data
                {
                    do
                    {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        let items = json as? [String : AnyObject]
                        
                        if let items = items
                        {
                            let dictionaries = items["items"] as? [AnyObject] ?? [AnyObject]()
                            for dictionary in dictionaries as? Array<Dictionary<String,AnyObject>> ?? [Dictionary<String,AnyObject>]()
                            {
                                let question = Question(jsonDictionary: dictionary)
                                questionResult.append(question)
                            }
                        }
                    }
                    catch
                    {
                        print("We didnt get the data : Something went wrong\(url)")
                    }
                }
                else
                {
                    receivedError = error
                    print("An error occurred \(String(describing: error))")
                }
                completionHandler(questionResult, receivedError)
            })
        }
    }
    
    func fetchData(url: URL, completionHandler: @escaping (Data?, Error?) -> Void )
    {
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            completionHandler(data, error)
        }
        task.resume()
    }
    
    func buildURLWithComponents(query: String) -> URL?
    {
        var stackExchangeURL =  URLComponents(string: "https://api.stackexchange.com/2.2/questions")
        stackExchangeURL?.queryItems = [URLQueryItem(name: "pagesize", value: "20"),
                                        URLQueryItem(name: "page", value: "1"),
                                        URLQueryItem(name: "order", value: "desc"),
                                        URLQueryItem(name: "sort", value: "activity"),
                                        URLQueryItem(name: "tagged", value: query),
                                        URLQueryItem(name: "site", value: "stackoverflow"),
                                        URLQueryItem(name: "filter", value: "withbody")]
        
        return stackExchangeURL?.url
    }
}
